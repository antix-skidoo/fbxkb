Description: <short summary of the patch>
 TODO: Put a short summary on the line above and replace this paragraph
 with a longer explanation of this change. Complete the meta-information
 with other relevant fields (see below for details). To make it easier, the
 information below has been extracted from the changelog. Adjust it or drop
 it.
 .
 fbxkb (0.6-2.8) unstable; urgency=medium
 .
   * antiX buster build
   * New maintainer (Closes: #767970)
   * Fix parsing of keyboard info, to show proper "us" flag. (Closes: #412254)
   * Use g_strdup() before passing strings to g_hash_table_insert(),
      to avoid use-after-free.
   * Remove deprecated gtk_set_locale() and change deprecated gdk_window_lookup()
      to gdk_x11_window_lookup_for_display().
      This fixes "pointer-trouble-at-implicit" lintian error.
   * Don't unconditionally strip fbxkb binary (Closes: #436824)
   * Removed unnecessary dependencies.
   * Added homepage, debian/watch, machine-readable debian/copyright.
   * Switched to source format 3.0 (quilt)
   * Bumped Standards-Version to 3.9.6
Author: anticapitalista <antix@operamail.com>
Bug-Debian: https://bugs.debian.org/412254
Bug-Debian: https://bugs.debian.org/436824
Bug-Debian: https://bugs.debian.org/767970

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2020-02-11

--- fbxkb-0.6.orig/man/Makefile
+++ fbxkb-0.6/man/Makefile
@@ -20,9 +20,8 @@ clean:
 
 
 install: all
-#	install -d  $(PREFIX)/share/man/man1
-#	install -m 644 $(TARGET) $(PREFIX)/share/man/man1
+	install -d  $(PREFIX)/share/man/man1
+	install -m 644 $(TARGET) $(PREFIX)/share/man/man1
 
 uninstall:
-#	rm -f $(PREFIX)/share/man/man1/$(TARGET)
-
+	rm -f $(PREFIX)/share/man/man1/$(TARGET)
--- fbxkb-0.6.orig/man/fbxkb.1
+++ fbxkb-0.6/man/fbxkb.1
@@ -1,70 +1,25 @@
 .\" man page originally for the Debian/GNU Linux system
-.TH FBPANEL "1" "February 2004" "fbxkb 2.2" "User Commands"
+.TH FBXKB "1" "January 2007" "fbxkb 0.6" "User Commands"
 .SH NAME
-fbxkb \- a lightweight GTK2-based panel for UNIX desktop.
+fbxkb \- a lightweight X11 keyboard switcher.
 .SH SYNOPSIS
 .B fbxkb
-[\fIOPTION\fR]
 .br
 .SH DESCRIPTION
 .PP
-fbxkb is desktop panel which provides graphical information and feedback about 
-desktop activity and allows interaction with the window manager. 
-It features:
-.HP
-\(bu taskbar \- shows a list of the managed windows (tasks)
-.HP
-\(bu pager \- thumbnailed view of the desktop.
-.HP
-\(bu launchbar \- buttons to quickly launch applications
-.HP
-\(bu show desktop \- button to iconify or shade all windows 
-.HP
-\(bu image \- display an image
-.HP
-\(bu clock \- show the current time and/or date
-.HP
-\(bu system tray \- tray for XEMBED icons (aka docklets)
-.PP
-fbxkb requires NETWM (www.freedesktop.org) compliant window manager. 
-You can run many instances of fbxkb each with its own configuration
-(see \fBOPTIONS\fR below).
+fbxkb is X11 keyboard switcher, which provides visual information 
+about current keyboard. It shows a flag of current keyboard in a 
+systray area and allows you to switch to another one.
+
+fbxkb requires NETWM (www.freedesktop.org) compliant window manager.
+It's written in C and uses the GTK+-2.4 library only (no GNOME is needed). 
 
 Most updated info about fbxkb can be found on its home page:
 http://fbxkb.sf.net/
-
 .SH OPTIONS
 .TP
-\fB\-h\fR
-\- print help message and exit.
-.TP
-\fB\-v\fR
-\- print version and exit.
-.TP
-\fB\-p <name>\fR 
-\- use the profile <name>. The profile is loaded from the file ~/.fbxkb/<name>.
-If that fails, fbxkb will load PREFIX/share/fbxkb/<name>. No \fB\-p\fR option is equivalent
-to \fB\-p default\fR 
-.SH CUSTOMIZATION
-To change default settings, copy profile file to your home directory
-.br
-      mkdir -p ~/.fbxkb
-      cp PREFIX/share/fbxkb/default ~/.fbxkb
-.br
-and edit it. Default profile file contains comments and explanation inside,
-so it should be easy. For full list of options please visit fbxkb's home page.
-
-.SH FILES
-.TP
-PREFIX/share/fbxkb
-Directory with system-wide resources and default settings
-.TP
-~/.fbxkb/
-Directory with the user's private profiles
-.TP
-~/.fbxkb/default
-The user's default profile.
+fbxkb hasn't options. It just works.
 .SH AUTHOR
 fbxkb was written by Anatoly Asviyan <aanatoly@users.sf.net>.
 This manual page was originally written for the
-Debian GNU/Linux system by Shyamal Prasad <shyamal@member.fsf.org>.
+Debian GNU/Linux system by Vadim Vatlin <vatlin@sthbel.ru>.
